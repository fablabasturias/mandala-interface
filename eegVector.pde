class eegVector{
  int timeStamp;
  int[] data;
  
  public eegVector(int timeStamp, int[] data){
    this.timeStamp = timeStamp;
    this.data = data;
  }
}