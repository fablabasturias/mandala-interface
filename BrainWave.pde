class BrainWave{
  /************************************************/
  // A dynamic ArrayList that stores the wave
  /************************************************/
  //DataValues
  // RawData - the white wave on visualiser
  int rawData = 0;
  // Blink
  int blinkStrength = 0;
  // Attention
  int attention= 0;
  // Meditation
  int meditation= 0;
  int meditationEasing = 0;
  // EEG Spectrums
  //setting up the indexes of the eeg spectrums
  private int delta = 0;
  private int theta = 1;
  private int low_alpha = 2;
  private int high_alpha = 3;
  private int low_beta = 4;
  private int high_beta = 5;
  private int low_gamma = 6;
  private int mid_gamma = 7;
  // Later get an Array List for historical data
  private int[] currentData = {0,0,0,0,0,0,0,0}; // initialise the data saving slot
  private int[] eegDataMax = {1,1,1,1,1,1,1,1}; // initialise the max data saving slot

  color dataColor = color(255);
  color[] waveColors = {
                color(255,0,0,102),
                color(143,0,255,102),
                color(255,82,202,200),
                color(255,165,6,200),
                color(173,105,7,200),
                color(175,255,133,200),
                color(56,142,0,200),
                color(255,224,0,200),
                color(6,167,247,200),
              };
              
  private int[] eegEasing = {0,0,0,0,0,0,0,0};


  ArrayList<eegVector> eegStream; // stores {timestamp, currentData[]}
  ArrayList<dataVector> attentionStream; //stores {timestamp, attention}
  ArrayList<dataVector> meditationStream; //stores {timestamp, meditation}
  ArrayList<dataVector> blinkStream; //stores {timestamp, blink}
  
  public BrainWave(){
    eegStream = new ArrayList<eegVector>();
    attentionStream = new ArrayList<dataVector>();
    meditationStream = new ArrayList<dataVector>();
    blinkStream = new ArrayList<dataVector>();
    
  }
  
  public int[] eegCurrent(){
    return currentData;
  }
  
  public void plot(int offsetX, int offsetY){
    int span = 30;
    //draw the sliders
    for(int i=0; i < eegEasing.length; i++){
      noStroke();
      //if(eegEasing[i]>currentData[i]){
      //  eegEasing[i] = eegEasing[i]-50;
      //}else{
        //eegEasing[i]=currentData[i];
      //}
      
      if(eegEasing[i] == 0){
        eegEasing[i]=currentData[i];
      }else{
        eegEasing[i] = (currentData[i]+eegEasing[i]*4)/5;
      }
      float sliderHeight = map(currentData[i], 0, eegDataMax[i], 3, 100);
      float sliderEasingHeight = map(eegEasing[i], 0, eegDataMax[i], 3, 100);
      fill(waveColors[i], 150);
      rect(offsetX-((eegEasing.length-1)*span/2)+i*span, offsetY+100-sliderEasingHeight, 6, sliderEasingHeight);
      fill(waveColors[i],255);
      rect(offsetX-((eegEasing.length-1)*span/2)+i*span, offsetY+100-sliderHeight, 6, 2);
    }
    
    //int blinkColor = floor(map(blinkStrength, 0, 100, 5, 255));
    //fill(blinkColor);
    //rect(offsetX-((eegEasing.length-1)*span/2), offsetY+120,(eegEasing.length-1)*span+6 ,3);
    
  }
  
  public void meditationDashboard(int x, int y, int r, int steps){
    //neutral is 50
    //calculate the angle from -90 degrees ~ 90 degrees
    if(meditationEasing == 0){
        meditationEasing = meditation;
      }else{
        meditationEasing = (meditation+meditationEasing*15)/16;
    }
    float angle = radians(((meditationEasing)*1.8)-180);
    //println(angle);
    float radX = cos(angle)*r;
    float radY = sin(angle)*r;
    //stroke(255);
    //strokeWeight(0.5);
    //line(x,y, radX+x, radY+y);
    noStroke();
    for(int i=0; i < steps; i++){
      float newX = cos((PI/(steps-1))*i-PI)*r;
      float newY = sin((PI/(steps-1))*i-PI)*r;
      float fillColor = (PI*r - dist(newX, newY, radX, radY)*5)/(PI*r);
      fill(255*fillColor);
      ellipse(x+newX,y+newY,10,10);
    }
    
    
  }
  
  public void UpdateEEG(int timeStamp,int delta, int theta, int low_alpha, int high_alpha, int low_beta, int high_beta, int low_gamma, int mid_gamma){
    
    currentData[this.delta] = delta;
    currentData[this.theta] = theta;
    currentData[this.low_alpha] = low_alpha;
    currentData[this.high_alpha] = high_alpha;
    currentData[this.low_beta] = low_beta;
    currentData[this.high_beta] = high_beta;
    currentData[this.low_gamma] = low_gamma;
    currentData[this.mid_gamma] = mid_gamma;
    
    //update for the max
    for(int i=0; i < currentData.length; i++){
      if(eegDataMax[i] < currentData[i]) eegDataMax[i] = currentData[i];
    }

    //push the data to the arraylist dataStream
    eegStream.add(new eegVector(timeStamp, currentData));
    
  }
  
  public void UpdateMax(int index, int value){
    if(eegDataMax[index] < value) eegDataMax[index] = value;
  }

  
  public void pushAttention(int timeStamp, int attention){
    this.attention = attention;
    attentionStream.add(new dataVector(timeStamp, attention));
  }
  
  public void pushMeditation(int timeStamp, int meditation){
    this.meditation = meditation;
    meditationStream.add(new dataVector(timeStamp, meditation));
  }
  
  public void pushBlink(int timeStamp, int blinkStrength){
    this.blinkStrength = blinkStrength;
    blinkStream.add(new dataVector(timeStamp, blinkStrength));
  }
  
  public void dumpJSON(String filename){
    //dump all the arraylists to the JSON file
    
  }
  

}