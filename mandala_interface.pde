
/*
   Process of connecting:

   1. Open Bluetooth setting on mac, make sure the headset is connected (by at the same time turn on the switch of the headset for pairing).
   2. ThinkGear is on
   3. Start the program

 */

import neurosky.*;
import org.json.*;
import processing.serial.*;

// G-Code
int NORMALSPEED = 1000;
int JUMPSPEED = 5000;
int PRINT_HEIGHT = 0;
boolean GO_UP_WHEN_JUMPING = true;
int GO_UP_DISTANCE = 2;
boolean CONCENTRIC_CIRCLES = true;
int PETRI_DIAMETER = 145;

// Printer Print Area settings - in mm
int PRINTER_X_OFFSET = 10;
int PRINTER_Y_OFFSET = 4;
int PRINTABLE_WIDTH = 194;
int PRINTABLE_HEIGHT = 195;
int PRINTER_BED_SIZE = 210;

String header = 
"G90\n" +                             // absolute positioning
"G21\n" +                             // millimeters
"M42 P4 S0\n" +                       // extruder sync off
"G1 Z40 F5000\n" +                    // move Z up 
"G28\n" +                             // home
"G1 Z40 F5000\n" +                    // move Z up again
"G1 X100 Y100 F5000\n";              // move to center
//"G1 X100 Y100 F5000\n" +              // move to center
//"G1 Z" + PRINT_HEIGHT + " F5000\n";   // move Z to print height

String footer = 
"M42 P4 S0\n" +                       // extruder sync off
"G1 Z40 F5000\n" +                    // move Z up
"G28 X0 Y0\n" +                       // home X and Y
"M84\n" +                             // stop motors
"M104 S0\n";                          // turn off heat


// Globals
Boolean fileOpened = false;
Button loadFileButton;
Button loadBacteriaButton;
Button printButton;
processing.data.XML svgcode;
int currentLine;

ArrayList<Circle> myCircles = new ArrayList<Circle>();
IntDict colorClasses;

int numberOfColors = 0;

// just send medidtation value if it changes
byte meditationToSend = 0;

// current operation
int processCounter = 0;
int currentGcode = 0;

ArrayList<String> gcodes;

// Printer Serial
Serial printerPort = null;
//String printerPortName = "/dev/ttyACM0"; // Linux
 String printerPortName = "/dev/cu.usbmodem14161"; // OSX
Boolean printerPortOpen = false;

// Extruder Serial
Serial extruderPort = null;
//String extruderPortName = "/dev/ttyACM1"; // Linux
String extruderPortName = "/dev/cu.usbmodem14111"; // OSX
Boolean extruderPortOpen = false;

String[] gcodeOutput;
int currentGcodeLine = 0;
Boolean sending = false;



//ControlP5 cp5;

ThinkGearSocket neuroSocket;

int initialTime;


int attention=10;
int meditation=10;
int blink = 0;
int currentSignalQuality = 200; // 200 is poor
int fullCircleSizeMajor = 80;
int fullCircleSizeMinor = 80;
PVector levelArea;

String[] controlValues = {"BlinkEvent","Attention","Meditation"};
String[] eegValues = {"delta", "theta", "low_alpha", "high_alpha", "low_beta", "high_beta", "low_gamma", "mid_gamma"};

BrainWave waveData = new BrainWave();

PFont font;
PShape brain;

int gWidth = 800;
int gHeight = 800;
int gOffsetX = 400;
int gOffsetY = 0;

int bWidth = 500;
int bHeight = 900;
int bOffsetX = 0;
int bOffsetY = 0;

void setup() {
  //fullScreen();
  size(displayWidth, displayHeight);

  levelArea = new PVector(width, height/3*2);

  ThinkGearSocket neuroSocket = new ThinkGearSocket(this);
  try {
    neuroSocket.start();
  } 
  catch (Exception e) {
    println("Is ThinkGear running??");
  }

  initialTime = millis();

  // color codes
  colorClasses = new IntDict(); 

  colorClasses.set("st0", 0xfffbf236);
  colorClasses.set("st1", 0xff99e550);
  colorClasses.set("st2", 0xff6abe30);
  colorClasses.set("st3", 0xff37946e);
  colorClasses.set("st4", 0xff4b692f);
  colorClasses.set("st5", 0xff524b24);
  colorClasses.set("st6", 0xff323c39);
  colorClasses.set("st7", 0xff3f3f74);

  colorClasses.set("st8", 0xff222222);
  colorClasses.set("st9", 0xff222034);
  colorClasses.set("st10", 0xff45283c);
  colorClasses.set("st11", 0xff663931);
  colorClasses.set("st12", 0xff8f563b);
  colorClasses.set("st13", 0xffdf7126);
  colorClasses.set("st14", 0xffd9a066);
  colorClasses.set("st15", 0xffeec39a);

  colorClasses.set("st16", 0xff306082);
  colorClasses.set("st17", 0xff5b6ee1);
  colorClasses.set("st18", 0xff639bff);
  colorClasses.set("st19", 0xff5fcde4);
  colorClasses.set("st20", 0xffcbdbfc);
  colorClasses.set("st21", 0xffffffff);
  colorClasses.set("st22", 0xff9badb7);
  colorClasses.set("st23", 0xff847e87);

  colorClasses.set("st24", 0xff696a6a);
  colorClasses.set("st25", 0xff595652);
  colorClasses.set("st26", 0xff76428a);
  colorClasses.set("st27", 0xffac3232);
  colorClasses.set("st28", 0xffd95763);
  colorClasses.set("st29", 0xffd777b2);
  colorClasses.set("st30", 0xff8f974a);
  colorClasses.set("st31", 0xff8a6f30);

  //smooth();

  loadFileButton= new Button((gWidth/2)-100/2+gOffsetX, (gHeight/2)-50/2+gOffsetY, 100,50, "Load File");
  loadBacteriaButton = new Button(gWidth-110+gOffsetX, 10, 100, 50, "");
  printButton = new Button(gWidth-110+gOffsetX, 10, 100, 50, "");

  // printer Serial port
  printerPort = new Serial(this, printerPortName, 115200);
  printerPortOpen = true;

  // extruder Serial port
  extruderPort = new Serial(this, extruderPortName, 115200);
  extruderPortOpen = true;
  
  //calculate the new printer offset
  

  //pixelDensity(2);
  textAlign(CENTER);
  smooth();
  brain = loadShape("brain.svg");
  brain.disableStyle();
  font = createFont("ArcherPro-Book",12);
  textFont(font);
  background(0,0,0);
}

void draw() {
  background(0,0,0);

  //draw signal quality bar

  //cp5.getController("SignalQuality").setValue(currentSignalQuality);
  noStroke();
  if(currentSignalQuality > 0){
    fill(255,222,0);
    rect(bWidth/2-50+bOffsetX,100+bOffsetY,100-map(currentSignalQuality,0,200,0,100),6);
  }else{
    fill(0,232,131);
    rect(bWidth/2-50+bOffsetX,100+bOffsetY,100,6);
  }

  stroke(255,255,255);
  strokeWeight(1);
  noFill();
  rect(bWidth/2-50+bOffsetX,100+bOffsetY,100,6);
  noStroke();
  fill(255);
  text("SIGNAL QUALITY", bWidth/2+bOffsetX, 125+bOffsetY);

  //print meditation level here

  if(currentSignalQuality > 0){
    //bad meditation value.
    noFill();
    stroke(255);
  }else{
    //show meditation value
    noStroke();
    fill(map(meditation,0,100,50,255));
  }
  shape(brain,bWidth/2-(brain.width/2),bHeight/2-(brain.height/2));

  waveData.plot(bWidth/2, 600);
  waveData.meditationDashboard(bWidth/2+bOffsetX, bHeight/2+bOffsetY+30, 160,20);

  // send meditation value?
  if (meditationToSend != byte(meditation)) {
    meditationToSend = byte(meditation);
    extruderPort.write(meditationToSend);
  }

  //Start the part of gcode and mandala

  switch (processCounter) {
    case 0:     // load file
      loadFileButton.show();
      break;
    case 1:     // show file and wait
      drawCircles();
      break;
    case 2:     // show n step and load bacteria
      drawCirclesStep(currentGcode);
      loadBacteriaButton.btext = "Load Bacteria #" + currentGcode;
      loadBacteriaButton.show();
      break;
    case 3:     // show n step and wait for print
      drawCirclesStep(currentGcode);
      printButton.btext = "Print Bacteria #"+ currentGcode;
      printButton.show();
      break;
    case 4:     // print
      drawCirclesStep(currentGcode);
      printButton.btext = "Printing... #" + currentGcode;
      printButton.show();
      break;

  }

}

void poorSignalEvent(int sig) {
  currentSignalQuality = sig;
  println("SignalEvent "+sig);
}

public void attentionEvent(int attentionLevel) {
  int timeStamp = millis() - initialTime;
  attention = attentionLevel;
  waveData.pushAttention(timeStamp,attention);
  //println("Attention Level: " + attentionLevel);
}


void meditationEvent(int meditationLevel) {
  int timeStamp = millis() - initialTime;
  meditation = meditationLevel;
  waveData.pushMeditation(timeStamp,meditation);
  println("Meditation Level: " + meditationLevel);
}

void blinkEvent(int blinkStrength) {
  int timeStamp = millis() - initialTime;
  blink = blinkStrength;
  waveData.pushBlink(timeStamp,blink);
  //println("blinkStrength: " + blinkStrength);
}

public void eegEvent(int delta, int theta, int low_alpha, int high_alpha, int low_beta, int high_beta, int low_gamma, int mid_gamma) {
  int timeStamp = millis() - initialTime;
  waveData.UpdateEEG(timeStamp, delta, theta, low_alpha, high_alpha, low_beta, high_beta, low_gamma, mid_gamma);
  //println("timeStamp:" + timeStamp);
  //println("delta Level: " + delta);
  //println("theta Level: " + theta);
  //println("low_alpha Level: " + low_alpha);
  //println("high_alpha Level: " + high_alpha);
  //println("low_beta Level: " + low_beta);
  //println("high_beta Level: " + high_beta);
  //println("low_gamma Level: " + low_gamma);
  //println("mid_gamma Level: " + mid_gamma);

}

void rawEvent(int[] raw) {
  //println(raw); 
  // raw data is a array with 512 values.
}  

void stop() {
  neuroSocket.stop();
  super.stop();
}


// CALLBACKS //////////////////////////

void openCode() {
  File file = null;
  selectInput("Select a SVG file:", "fileSelected", file);
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    svgcode = loadXML(selection.getAbsolutePath());
    fileOpened=true;
    findColors();
    findCircles();
    gcodesGen();
    saveStrings("gcode1.gcode", gcodes.get(0).split("\n"));
    // ok, next step
    processCounter = 1;
  }
}

void mousePressed() {
  if (loadFileButton.overButton() && processCounter == 0) {
    // first step
    openCode();
  }
  if (processCounter == 1) {
    // show the current bacteria
    processCounter++;
    return;
  }

  if (loadBacteriaButton.overButton() && processCounter == 2) {
    loadBacteria();
    processCounter++;
    return;
  }

  if (printButton.overButton() && processCounter == 3) {
    printBacteria();
    return;
  }


}

// get the number of colors in the drawing
void findColors() {
  processing.data.XML style = svgcode.getChild("style");
  String styles[] = style.getContent().trim().split("\n");
  numberOfColors = styles.length;
  println(numberOfColors);
}

// find all the circles in the drawing and store them
void findCircles() {
  processing.data.XML[] circles = svgcode.getChildren("g/g/circle");

  // clean the circles array
  myCircles.clear();

  for (int i=0; i < circles.length; i++) {
    String cs = circles[i].getString("class");
    float x = circles[i].getFloat("cx");
    float y = circles[i].getFloat("cy");
    float r = circles[i].getFloat("r");

    myCircles.add(new Circle(x,y,r,colorClasses.get(cs)));
  }
}

// draw the circles, full color
void drawCircles() {

  // get viewbox
  String viewBox = svgcode.getString("viewBox");
  int vx, vy, vw, vh;
  vx = int(viewBox.split(" ")[0]);
  vy = int(viewBox.split(" ")[1]);
  vw = int(viewBox.split(" ")[2]);
  vh = int(viewBox.split(" ")[3]);

  // draw the circles mapping the coordinates to the window
  for(int i=0; i< myCircles.size(); i++) {
    Circle c = myCircles.get(i);
    float x,y,r;
    x = map(c.x, vx, vx+vw, 0, gWidth)+gOffsetX;
    y = map(c.y, vy, vy+vh, 0, gHeight)+gOffsetY;
    r = c.radius; 
    noStroke();
    fill(c.cl);
    ellipse(round(x),round(y),r,r);
  }
}

// draw the circles, current step highlighted
void drawCirclesStep(int n) {

  // get viewbox
  String viewBox = svgcode.getString("viewBox");
  int vx, vy, vw, vh;
  vx = int(viewBox.split(" ")[0]);
  vy = int(viewBox.split(" ")[1]);
  vw = int(viewBox.split(" ")[2]);
  vh = int(viewBox.split(" ")[3]);

  // draw the circles mapping the coordinates to the window
  for(int i=0; i< myCircles.size(); i++) {
    Circle c = myCircles.get(i);
    float x,y,r;
    x = map(c.x, vx, vx+vw, 0, gWidth)+gOffsetX;
    y = map(c.y, vy, vy+vh, 0, gHeight)+gOffsetY;
    r = c.radius; 
    noStroke();
    if (c.cl == colorClasses.valueArray()[n])
      fill(255);
    else
      fill(50);
    ellipse(round(x),round(y),r,r);
  }
}


void gcodesGen() {

  gcodes = new ArrayList<String>();

  // get viewbox
  String viewBox = svgcode.getString("viewBox");
  int vx, vy, vw, vh;
  vx = int(viewBox.split(" ")[0]);
  vy = int(viewBox.split(" ")[1]);
  vw = int(viewBox.split(" ")[2]);
  vh = int(viewBox.split(" ")[3]);

  // for each color
  for (int i = 0; i < numberOfColors; i++) {
    // start
    String gcode = "";
    gcode += header;

    // search circles
    for (int j = 0; j < myCircles.size(); j++) {
      Circle c = myCircles.get(j);

      // if current color
      if (c.cl == colorClasses.valueArray()[i]) {
        // generate g-code for that circle
        float x, y, r;
        // map coordinates
        x = map(c.x, vx, vx+vw, 0, PETRI_DIAMETER)+((PRINTER_BED_SIZE/2)-PRINTER_X_OFFSET-(PETRI_DIAMETER/2));
        y = map(c.y, vy, vy+vh, 0, PETRI_DIAMETER)+((PRINTER_BED_SIZE/2)-PRINTER_Y_OFFSET-(PETRI_DIAMETER/2));
        //r = map(c.radius, 0, vw, 0, PETRI_DIAMETER)/2;
        r = c.radius/2;

        if (GO_UP_WHEN_JUMPING)
          // up
          gcode += "G1 Z" + GO_UP_DISTANCE + " F5000 \n";

        // move to the center
        // we move to the center, but on X we substract the radius
        // so we start and end the circle there
        gcode += "G1 F" + JUMPSPEED + " X" + Float.toString(x-r) + " Y" + Float.toString(y) + " E1 \n";

        if (GO_UP_WHEN_JUMPING)
          // down
          gcode += "G1 Z" + PRINT_HEIGHT + " F5000 \n";

        // now make the circle
        gcode += "M400\n"; // wait for movement commands to finish
        gcode += "M42 P4 S255\n"; // turn on pin P4

        // start and end points are the same
        gcode += "G2 I" + Float.toString(r) 
          + " J0" + " F" + NORMALSPEED 
          + " E5 \n";

        if (CONCENTRIC_CIRCLES) {
          for (float cc = r-1; cc > 0; cc--) {
            // move to next initial point
            gcode +=  "G1 F" + JUMPSPEED + " X" + Float.toString(x-cc) + " Y" + Float.toString(y) + " E1 \n";
            // make the circle
            gcode += "G2 I" + Float.toString(cc) 
              + " J0" + " F" + NORMALSPEED 
              + " E5 \n";
          }
        }

        gcode += "M400\n";
        gcode += "M42 P4 S0\n"; // turn off P4

      }
    }

    // end
    gcode += footer;

    // add gcode to gcodes list
    gcodes.add(gcode);
  }

}

// loads bacteria for printing
void loadBacteria() {
}


// start printing
void printBacteria() {
  // print
  processCounter = 4;

  // get current code
  gcodeOutput = gcodes.get(currentGcode).split("\n");

  // send it
  sendCode();
}


void nextGcode() {
  // check if there are more G-Codes to print
  if (currentGcode < gcodes.size()-1) {
    currentGcode++;     // next GCode
    processCounter = 2; // show next bacteria to print
  }
  else {
    // start again
    currentGcode = 0;
    processCounter = 0;
  }

}

void sendCode() {
  if (gcodeOutput==null) 
    return;

  sending = true;
  sendCodeLine();
}

// sends a gcode line
void sendCodeLine() {
  if (!printerPortOpen) return;
  if (!sending) return;

  // check if we are done
  if (currentGcodeLine == gcodeOutput.length) {
    sending = false;
    currentGcodeLine = 0;
    nextGcode();  // next gcode?
    return;
  }

  // ok, get next line
  String line = gcodeOutput[currentGcodeLine];
  // send it
  println(line);
  printerPort.write(line + "\n");

  // next
  currentGcodeLine++;
}

// receives serial events
void serialEvent(Serial p) { 
  try {
    String s = p.readStringUntil('\n');
    println(s.trim());

    if (s.trim().startsWith("ok"))
      sendCodeLine();

    if (s.trim().startsWith("error"))
      sending = false;

  }
  catch (Exception e)
  {
    // Port not opened
  }
}