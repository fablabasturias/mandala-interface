class Button {
    int x, y, w, h;
    String btext;
    color bfill;
    color bstroke;

    Button(int nx, int ny, int nw, int nh, color nfill, color nstroke, String ntext) {
        x=nx;
        y=ny;
        w=nw;
        h=nh;
        btext=ntext;
        bfill=nfill;
        bstroke=nstroke;
    }

    Button(int nx, int ny,int nw, int nh, String ntext) {
        x=nx;
        y=ny;
        w=nw;
        h=nh;
        btext=ntext;
        bfill=0;
        bstroke=255;
    }

    void show() {
        fill(bfill);
        stroke(bstroke);
        rect(x,y,w,h);
        textSize(12);
        textAlign(CENTER);
        fill(bstroke);
        text(btext,x+(w/2), (y+(h/2))+12/2);
    }

    boolean overButton()  {
        if (mouseX >= x && mouseX <= x+w && 
                mouseY >= y && mouseY <= y+h) {
            return true;
        } else {
            return false;
        }
    }
}


class Circle {
    float x, y, radius;
    int cl;             // color

    Circle(float nx, float ny, float nradius, int nc) {
        x = nx;
        y = ny;
        radius = nradius;
        cl = nc;
    }
}